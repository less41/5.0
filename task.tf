variable "countvm" {
  default = "3"
}
variable "indx" {
 default = "0"
}

resource "google_compute_instance"  "vm_instance"  {
  count = "${var.countvm}"
  name = "node${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params  {
     size = "20"
     image = "debian-cloud/debian-10"
     }
    }
     network_interface {
     network = "default"
     access_config {
    }
   }
}
